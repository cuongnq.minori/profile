import React from "react";
import "./App.scss";

function App() {
  return (
    <section className="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1 revealator-within">
      <div className="color-block d-none d-lg-block"></div>
      <div className="row home-details-container align-items-center">
        <div className="col-lg-4 bg position-fixed d-none d-lg-block"></div>
        <div className="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
          <div>
            <img
              src="http://slimhamdi.net/tunis/dark/img/img-mobile.jpg"
              className="img-fluid main-img-mobile d-none d-sm-block d-lg-none"
              alt=""
            />
            <h6 className="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">
              hi there !
            </h6>
            <h1 className="text-uppercase poppins-font">
              <span>I'm</span> steve milner
            </h1>
            <p className="open-sans-font">
              I'm a Tunisian based web designer &amp; front‑end developer
              focused on crafting clean &amp; user‑friendly experiences, I am
              passionate about building excellent software that improves the
              lives of those around me.
            </p>
            <a href="about.html" className="btn btn-about">
              <span data-hover="more about me">more about me</span>
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}

export default App;
